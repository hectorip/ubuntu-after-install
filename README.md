
# ubuntu-after-install

Aplicaciones útiles despuesués de instalar ubuntu.


## Pre-requisitos

* Python 3^ [Ya viene instalado en muchas versiones ]
* Ansible 2.8^ - https://www.ansible.com/resources/get-started


## Instalar ansible
~~~
sudo apt-get install ansible
~~~

## Instalar roles (galaxy)
Postman galaxy: `ansible-galaxy install gantsign.postman`

## Correr playbook
~~~
ansible-playbook playbook.yaml -i "localhost," -c local -b -K
~~~

## (Opcional) Correr playbook por etiquetas

~~~
ansible-playbook playbook.yaml -i "localhost," -c local -b -K --tags "apt"
~~~